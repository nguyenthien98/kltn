import check from 'is_js';
import jwt from 'jsonwebtoken';
import db from './adapters/db';

const validateEmployee = async (req, res, next) => {
  const { authorization } = req.headers;
  let flag = false;
  if (check.string(authorization)) {
    const bearerArr = authorization.split(' ');
    const bearerStr = bearerArr[1];
    const decoded = jwt.verify(bearerStr, 'secret');
    const checkEmail = await db('employee')
      .where({ email: decoded.data })
      .first('id', 'email', 'role_id');
    if (checkEmail) {
      if (checkEmail.role_id === '3') {
        req.email = checkEmail.email;
        req.userId = checkEmail.id;
        flag = true;
      }
    }
  }
  req.login = flag;
  return next();
};
const validateManager = async (req, res, next) => {
  const { authorization } = req.headers;
  let flag = false;
  if (check.string(authorization)) {
    const bearerArr = authorization.split(' ');
    const bearerStr = bearerArr[1];
    const decoded = jwt.verify(bearerStr, 'secret');
    const checkEmail = await db('employee')
      .where({ email: decoded.data })
      .first('id', 'email', 'role_id', 'firstname', 'lastname');
    if (checkEmail) {
      flag = true;
      req.userId = checkEmail.id;
      req.email = checkEmail.email;
      req.role_id = checkEmail.role_id;
      req.firstname = checkEmail.firstname;
      req.lastname = checkEmail.lastname;
    }
  }
  req.login = flag;
  return next();
};
const validateAdmin = async (req, res, next) => {
  const { authorization } = req.headers;
  let flag = false;
  if (check.string(authorization)) {
    const bearerArr = authorization.split(' ');
    const bearerStr = bearerArr[1];
    const decoded = jwt.verify(bearerStr, 'secret');
    const checkEmail = await db('employee')
      .where({ email: decoded.data })
      .first('id', 'email', 'role_id');

    if (checkEmail) {
      if (checkEmail.role_id === 1) {
        flag = true;
        req.userId = checkEmail.id;
        req.email = checkEmail.email;
        req.role_id = checkEmail.role_id;
      }
    }
  }
  req.login = flag;
  return next();
};
const validate = async (req, res, next) => {
  const { authorization } = req.headers;
  let flag = false;
  if (check.string(authorization)) {
    const bearerArr = authorization.split(' ');
    const bearerStr = bearerArr[1];
    const decoded = jwt.verify(bearerStr, 'secret');
    const checkEmail = await db('employee')
      .where({ email: decoded.data })
      .first('id', 'firstname', 'lastname', 'email', 'role_id');

    if (checkEmail) {
      flag = true;
      req.userId = checkEmail.id;
      req.email = checkEmail.email;
      req.firstname = checkEmail.firstname;
      req.lastname = checkEmail.lastname;
      req.role_id = checkEmail.role_id;
    }
  }
  req.login = flag;
  return next();
};
export default {
  validateEmployee,
  validateManager,
  validateAdmin,
  validate,
};
