import express from 'express';
import multer from 'multer';
import cloudinaryStorage from 'multer-storage-cloudinary';
import staff from './controllers/staff/staff';
import middleware from './middleware';
import manager from './controllers/manager/manager';
import bank from './controllers/bank';
import project from './controllers/project';
import seat from './controllers/seating-plan';
import userManagement from './controllers/user-management';
import insurance from './controllers/user-management/insurance';
import bankAccount from './controllers/user-management/bank-account';
import contract from './controllers/user-management/contract';
import feedback from './controllers/user-management/feedback';
import avatar from './controllers/user-management/avatar';
import task from './controllers/task';
import projectInfo from './controllers/user-management/project-info';
import leave from './controllers/user-management/leave';
import cloudinary from './adapters/image';
import salary from './controllers/salary';
import exportSalary from './controllers/salary/export';
import reportSalary from './controllers/salary/send-report';
import otherSalary from './controllers/salary/other';

const router = express.Router();

const storage = cloudinaryStorage({
  cloudinary,
  params: {
    folder: 'avatar',
    allowed_formats: ['jpg', 'png', 'jpeg'],
  },
});

const upload = multer({ storage });

router.post('/login', staff.login);
router.post('/change-password/:email', middleware.validateEmployee, staff.changePassword);
router.post('/manager/create/department', middleware.validateAdmin, manager.createDepartment);
router.post('/manager/update/department', middleware.validateAdmin, manager.updateDepartment);
router.post('/manager/create/contract', middleware.validateManager, manager.createContract);
router.post('/manager/update/contract', middleware.validateManager, manager.updateContract);
router.get('/manager/list-all-department', middleware.validateManager, manager.listDepartment);

router.get('/banks', middleware.validate, bank.getList);
router.post('/banks', middleware.validateManager, bank.save);

router.get('/projects', middleware.validate, project.getList);

router.get('/seats', middleware.validate, seat.getList);
router.post('/seats', middleware.validate, seat.save);

router.get('/users', middleware.validate, userManagement.getList);
router.get('/users/:userId', middleware.validate, userManagement.get);
router.delete('/users/:userId', middleware.validate, userManagement.deleteUser);
router.post('/users', middleware.validateManager, userManagement.create);
router.put('/users', middleware.validate, userManagement.update);
router.post('/users/:userId/insurances', middleware.validateManager, insurance.update);
router.get('/users/:userId/bank-accounts', middleware.validate, bankAccount.getListBankAccount);
router.post('/users/:userId/bank-accounts', middleware.validateManager, bankAccount.save);
router.post('/users/:userId/contracts', middleware.validateManager, contract.save);
router.post('/users/:userId/feedbacks', middleware.validateManager, feedback.save);
router.post('/users/:userId/avatar', middleware.validate, upload.single('avatar'), avatar.upload);
router.post('/users/:userId/projects', middleware.validate, projectInfo.save);

router.get('/users/:userId/leaves/:year', middleware.validate, leave.list);
router.post('/users/:userId/leaves', middleware.validate, leave.create);
router.put('/users/:userId/leaves/:leaveId/cancel', middleware.validate, leave.cancel);

router.get('/salaries', middleware.validateManager, salary.getList);
router.post('/salaries', middleware.validateManager, salary.save);
router.post('/salaries/export', middleware.validateManager, exportSalary.exportSalaries);
router.post('/salaries/reports/send', middleware.validateManager, reportSalary.send);
router.post('/salaries/others', middleware.validateManager, otherSalary.update);

router.get('/users/:userIds/tasks', middleware.validate, task.list);
router.post('/users/:userIds/tasks/:type', middleware.validate, task.save);

export default router;
