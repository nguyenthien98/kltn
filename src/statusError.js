const helper = {
  badRequest(res, obj) {
    res.status(400).json({ obj });
  },
  notfound(res, obj) {
    res.status(404).json({ obj });
  },
  internalServerError(res, obj) {
    res.status(500).json({ obj });
  },
  success(res, obj) {
    res.status(200).json({ obj });
  },
  unauthorized(res, obj) {
    res.status(501).json({ obj });
  },
};
export default helper;
