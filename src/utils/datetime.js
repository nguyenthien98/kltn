import moment from 'moment';

function getTotalWorkDays(startDate, endDate) {
  const day = moment(startDate);
  let businessDays = 0;

  while (day.isSameOrBefore(endDate, 'day')) {
    if (day.day() !== 0 && day.day() !== 6) businessDays += 1;
    day.add(1, 'd');
  }
  return businessDays;
}

const getAllDates = (startDate, endDate) => {
  const date = moment(startDate);
  const days = [];
  while (date.isSameOrBefore(endDate, 'day')) {
    days.push(moment(date));
    date.add(1, 'd');
  }

  return days;
};

export default { getTotalWorkDays, getAllDates };
