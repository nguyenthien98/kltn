import nodemailer from 'nodemailer';
import moment from 'moment';
import fs from 'fs';

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  Port: 465,
  secure: true,
  auth: {
    user: process.env.EMAIL_SUPPORT,
    pass: process.env.PASS_SUPPORT,
  },
});

transporter.verify((error) => {
  if (error) throw error;
});

const sendSalaryReport = (time, sendTo, filename, path) => {
  const mailOptions = {
    from: 'hr@human-resource.com',
    to: sendTo,
    subject: `Salary report for ${moment(`${time}-01`).format('MMM YYYY')}`,
    html: '<b>Salary report</b>',
    attachments: [
      {
        filename,
        path,
      },
    ],
  };

  transporter.sendMail(mailOptions, (error) => {
    if (error) {
      throw error;
    } else {
      fs.unlink(path, (err) => {
        if (err) throw err;
      });
    }
  });
};

export default { sendSalaryReport };
