import { v1 as uuidv1 } from 'uuid';
import helper from '../../statusError';
import db from '../../adapters/db';

// const {
//   NODE_ENV,
//   // EMAIL_SUPPORT, PASS_SUPPORT, DOMAIN_DRIVEN_EMAIL,
// } = process.env;

const createDepartment = async (req, res) => {
  try {
    const id = uuidv1();
    const { name, location, amountEmployee } = req.body;
    await db('department').insert({
      id,
      name,
      location,
      amount_employee: amountEmployee,
    });
    return helper.success(res, 'success');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const updateDepartment = async (req, res) => {
  try {
    const { id } = req.params;
    const { userId, position } = req.body;
    await db('department').update({ user_id: userId, position }).where({ id });
    return helper.success(res, 'success');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const listDepartment = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');
  try {
    const list = await db('department').select();
    return helper.success(res, list);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const createContract = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');
  const { userId } = req;
  try {
    const id = uuidv1();
    const date = new Date();
    const { state, contractTerm, signingDate } = req.body;
    if (!state) return helper.badRequest(res, 'ERR_COMMON');
    if (!contractTerm) return helper.badRequest(res, 'ERR_COMMON');
    if (!signingDate) return helper.badRequest(res, 'ERR_COMMON');
    await db('contract').insert({
      id,
      employee_id: userId,
      state,
      contract_term: contractTerm,
      signing_date: signingDate,
      created_at: date,
      updated_at: date,
    });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const updateContract = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');
  try {
    const { id } = req.params;
    const data = new Date();
    const { state, contractTerm, signingDate } = req.body;
    if (!state) return helper.badRequest(res, 'ERR_COMMON');
    if (!contractTerm) return helper.badRequest(res, 'ERR_COMMON');
    if (!signingDate) return helper.badRequest(res, 'ERR_COMMON');
    await db('contract')
      .update({
        state,
        contract_term: contractTerm,
        signing_date: signingDate,
        updated_at: data,
      })
      .where({ id });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const listContract = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');

  try {
    const getall = await db('contract').select();
    return helper.success(res, getall);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const createRewardDiscipline = async (req, res) => {
  try {
    const { employeeId, ratio, reason, status, date } = req.body;
    if (!employeeId) return helper.badRequest(res, 'ERR_COMMON');
    if (!ratio) return helper.badRequest(res, 'ERR_COMMON');
    if (!reason) return helper.badRequest(res, 'ERR_COMMON');
    if (!status) return helper.badRequest(res, 'ERR_COMMON');
    if (!date) return helper.badRequest(res, 'ERR_COMMON');

    await db('reward_discipline').insert({
      employee_id: employeeId,
      ratio,
      reason,
      status,
      date,
    });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const updateRewardDiscipline = async (req, res) => {
  try {
    const { id } = req.params;
    const { ratio, reason, status, date } = req.body;
    if (!ratio) return helper.badRequest(res, 'ERR_COMMON');
    if (!reason) return helper.badRequest(res, 'ERR_COMMON');
    if (!status) return helper.badRequest(res, 'ERR_COMMON');
    if (!date) return helper.badRequest(res, 'ERR_COMMON');

    await db('reward_discipline')
      .update({
        ratio,
        reason,
        status,
        date,
      })
      .where({ id });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const listRewardDiscipline = async (req, res) => {
  try {
    const getall = await db('employee')
      .leftJoin('tax', 'tax.employee_id', 'employee.id')
      .leftJoin(
        'reward_discipline',
        'employee.id',
        'reward_discipline.employee_id'
      )
      .leftJoin('basic_salary', 'employee.id', 'basic_salary.employee_id')
      .leftJoin('department', 'employee.id', 'department.userId')
      .select(
        'employee.id',
        'employee.firstname',
        'employee.lastname',
        'employee.email',
        'employee.address',
        'employee.birthday',
        'employee.phone',
        'employee.gender',
        'employee.identity_card',
        'employee.employee_code',
        'basic_salary.id as basic_salary_id',
        'basic_salary.ratio as basic_salary_ratio',
        'basic_salary.money as basic_salary_money',
        'department.id as department_id',
        'department.name as department_name',
        'department.position as department_position',
        'department.location as department_location',
        'department.amount_employee',
        'tax.id as tax_id',
        'tax.code as tax_code',
        'tax.ratio as tax_ratio',
        'reward_discipline.id as reward_discipline_id',
        'reward_discipline.ratio as reward_discipline_ratio',
        'reward_discipline.reason as reward_discipline_reason',
        'reward_discipline.status as reward_discipline_status',
        'reward_discipline.date as reward_discipline_date'
      );
    return helper.success(res, getall);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const createTax = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');
  try {
    const { employeeId, code, ratio } = req.body;
    if (!code) return helper.badRequest(res, 'ERR_COMMON');
    if (!ratio) return helper.badRequest(res, 'ERR_COMMON');

    const checkId = await db('employee').where({ id: employeeId }).first();
    if (!checkId) return helper.badRequest(res, 'USER_INVALID');

    await db('tax').insert({
      employee_id: employeeId,
      code,
      ratio,
    });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const updateTax = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');
  try {
    const { id } = req.params;
    const { code, ratio } = req.body;
    if (!code) return helper.badRequest(res, 'ERR_COMMON');
    if (!ratio) return helper.badRequest(res, 'ERR_COMMON');

    const TaxEmployeeId = await db('employee').where({ id }).first();
    const checkEmployee = await db('employee')
      .where({ id: TaxEmployeeId.employee_id })
      .first();
    if (!checkEmployee) return helper.badRequest(res, 'USER_INVALID');

    await db('tax')
      .update({
        code,
        ratio,
      })
      .where({ id });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const listTax = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');
  try {
    const getall = await db('employee')
      .leftJoin('tax', 'tax.employee_id', 'employee.id')
      .leftJoin(
        'reward_discipline',
        'employee.id',
        'reward_discipline.employee_id'
      )
      .leftJoin('basic_salary', 'employee.id', 'basic_salary.employee_id')
      .leftJoin('department', 'employee.id', 'department.userId')
      .select(
        'employee.id',
        'employee.firstname',
        'employee.lastname',
        'employee.email',
        'employee.address',
        'employee.birthday',
        'employee.phone',
        'employee.gender',
        'employee.identity_card',
        'employee.employee_code',
        'basic_salary.id as basic_salary_id',
        'basic_salary.ratio as basic_salary_ratio',
        'basic_salary.money as basic_salary_money',
        'department.id as department_id',
        'department.name as department_name',
        'department.position as department_position',
        'department.location as department_location',
        'department.amount_employee',
        'tax.id as tax_id',
        'tax.code as tax_code',
        'tax.ratio as tax_ratio',
        'reward_discipline.id as reward_discipline_id',
        'reward_discipline.ratio as reward_discipline_ratio',
        'reward_discipline.reason as reward_discipline_reason',
        'reward_discipline.status as reward_discipline_status',
        'reward_discipline.date as reward_discipline_date'
      );
    return helper.success(res, getall);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const createInsurance = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');
  try {
    const { code, ratio, hospital, employeeId, dateEnd } = req.body;
    if (!code) return helper.badRequest(res, 'ERR_COMMON');
    await db('insurance').insert({
      code,
      employee_id: employeeId,
      ratio,
      hospital,
      date_end: dateEnd,
    });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const updateInsurance = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');
  try {
    const { id } = req.params;
    const { code, ratio, hospital, dateEnd } = req.body;
    if (!code) return helper.badRequest(res, 'ERR_COMMON');
    await db('insurance')
      .update({
        code,
        ratio,
        hospital,
        date_end: dateEnd,
      })
      .where({ id });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const listInsurance = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');
  try {
    const getall = await db('employee')
      .leftJoin('tax', 'tax.employee_id', 'employee.id')
      .leftJoin(
        'reward_discipline',
        'employee.id',
        'reward_discipline.employee_id'
      )
      .leftJoin('basic_salary', 'employee.id', 'basic_salary.employee_id')
      .leftJoin('department', 'employee.id', 'department.userId')
      .leftJoin('insurance', 'insurance.employee_id', 'employee.id')
      .select(
        'employee.id',
        'employee.firstname',
        'employee.lastname',
        'employee.email',
        'employee.address',
        'employee.birthday',
        'employee.phone',
        'employee.gender',
        'employee.identity_card',
        'employee.employee_code',
        'basic_salary.id as basic_salary_id',
        'basic_salary.ratio as basic_salary_ratio',
        'basic_salary.money as basic_salary_money',
        'department.id as department_id',
        'department.name as department_name',
        'department.position as department_position',
        'department.location as department_location',
        'department.amount_employee',
        'tax.id as tax_id',
        'tax.code as tax_code',
        'tax.ratio as tax_ratio',
        'reward_discipline.id as reward_discipline_id',
        'reward_discipline.ratio as reward_discipline_ratio',
        'reward_discipline.reason as reward_discipline_reason',
        'reward_discipline.status as reward_discipline_status',
        'reward_discipline.date as reward_discipline_date',
        'insurance.id as insurance_id',
        'insurance.code as insurance_code',
        'insurance.ratio as insurance_ratio',
        'insurance.hostpital as insurance_hostpital',
        'insurance.date_end'
      );
    return helper.success(res, getall);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const listDetailInsurance = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');
  try {
    const getall = await db('employee')
      .leftJoin('tax', 'tax.employee_id', 'employee.id')
      .leftJoin(
        'reward_discipline',
        'employee.id',
        'reward_discipline.employee_id'
      )
      .leftJoin('basic_salary', 'employee.id', 'basic_salary.employee_id')
      .leftJoin('department', 'employee.id', 'department.userId')
      .leftJoin('insurance', 'insurance.employee_id', 'employee.id')
      .first(
        'employee.id',
        'employee.firstname',
        'employee.lastname',
        'employee.email',
        'employee.address',
        'employee.birthday',
        'employee.phone',
        'employee.gender',
        'employee.identity_card',
        'employee.employee_code',
        'basic_salary.id as basic_salary_id',
        'basic_salary.ratio as basic_salary_ratio',
        'basic_salary.money as basic_salary_money',
        'department.id as department_id',
        'department.name as department_name',
        'department.position as department_position',
        'department.location as department_location',
        'department.amount_employee',
        'tax.id as tax_id',
        'tax.code as tax_code',
        'tax.ratio as tax_ratio',
        'reward_discipline.id as reward_discipline_id',
        'reward_discipline.ratio as reward_discipline_ratio',
        'reward_discipline.reason as reward_discipline_reason',
        'reward_discipline.status as reward_discipline_status',
        'reward_discipline.date as reward_discipline_date',
        'insurance.id as insurance_id',
        'insurance.code as insurance_code',
        'insurance.ratio as insurance_ratio',
        'insurance.hostpital as insurance_hostpital',
        'insurance.date_end'
      );
    return helper.success(res, getall);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const createproject = async (req, res) => {
  try {
    const id = uuidv1();
    const { client, name, startDate, endDate } = req.body;
    if (!client) return helper.badRequest(res, 'ERR_COMMON');
    if (!name) return helper.badRequest(res, 'ERR_COMMON');
    if (!startDate) return helper.badRequest(res, 'ERR_COMMON');
    if (!endDate) return helper.badRequest(res, 'ERR_COMMON');
    await db('employee').insert({
      id,
      name,
      client,
      startDate,
      endDate,
    });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

export default {
  createDepartment,
  createproject,
  updateDepartment,
  createContract,
  updateContract,
  listDepartment,
  listContract,
  createRewardDiscipline,
  updateRewardDiscipline,
  listRewardDiscipline,
  createTax,
  updateTax,
  listTax,
  createInsurance,
  updateInsurance,
  listInsurance,
  listDetailInsurance,
};
