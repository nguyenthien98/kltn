import helper from '../../statusError';
import db from '../../adapters/db';

const getList = async (req, res) => {
  const projects = await db('project').select('id', 'name', 'client', 'startDate', 'endDate');
  return helper.success(res, projects);
};

export default {
  getList,
};
