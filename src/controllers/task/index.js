/* eslint-disable operator-linebreak */
/* eslint-disable no-param-reassign */
/* eslint-disable comma-dangle */
import moment from 'moment';
import helper from '../../statusError';
import db from '../../adapters/db';

const list = async (req, res) => {
  const userIds =
    req.params.userIds === 'me' ? [req.userId] : req.params.userIds.split(',');
  const times = req.query.times
    ? req.query.times.split(',')
    : [moment().startOf('isoweek').format('YYYY-MM-DD')];

  const tasks = await db('task')
    .whereIn('employeeId', userIds)
    .whereIn('time', times)
    .select(
      'id',
      'activity',
      'task',
      'project',
      'time',
      'submitted',
      'employeeId'
    );

  const taskIds = tasks.map((item) => item.id);
  const details = await db('task_detail')
    .whereIn('taskId', taskIds)
    .select('id', 'taskId', 'workingDate', 'workingHour');

  return helper.success(res, { tasks, details });
};

const save = async (req, res) => {
  const userIds = req.params.userIds.split(',');
  const submitted = req.params.type === 'submit';
  const { tasks, details } = req.body;
  try {
    await db.transaction(async (trx) => {
      const { time } = tasks && tasks.length > 0 ? tasks[0] : req.query;
      const taskIds = await trx('task')
        .whereIn('employeeId', userIds)
        .where('time', time)
        .select('id');
      const ids = taskIds.map((item) => item.id);
      if (ids.length > 0) {
        await trx('task_detail').whereIn('taskId', ids).del();
        await trx('task').whereIn('id', ids).del();
      }

      const newTasks = tasks.map((item) => {
        item.submitted = submitted;
        return item;
      });

      if (newTasks && newTasks.length > 0) {
        await trx('task').insert(newTasks);
      }
      if (details && details.length > 0) {
        await trx('task_detail').insert(details);
      }
    });

    return helper.success(res);
  } catch(error) {
    console.log(error);
    return helper.internalServerError(res);
  }
};

export default { list, save };
