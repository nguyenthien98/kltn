import { v1 as uuid } from 'uuid';
import helper from '../../statusError';
import db from '../../adapters/db';

const getList = async (req, res) => {
  const banks = await db('bank').select('id', 'name');
  return helper.success(res, banks);
};

const save = async (req, res) => {
  try {
    const { id, name } = req.body;
    const entity = { name };
    if (id) {
      await db('bank').update(entity).where({ id });
    } else {
      entity.id = uuid();
      await db('bank').insert(entity);
    }
    return helper.success(res, entity);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

export default {
  getList,
  save,
};
