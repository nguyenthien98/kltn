import { v1 as uuid } from 'uuid';
import helper from '../../statusError';
import db from '../../adapters/db';

const getListBankAccount = async (req, res) => {
  const userId = req.params.userId === 'me' ? req.userId : req.params.userId;
  const banks = await db('bank_account')
    .where({ employeeId: userId })
    .left('bank', 'bank_account.bankId', 'bank.id')
    .select(
      'id',
      'employeeId',
      'bankId',
      'bank.name as bankName',
      'accountNumber',
      'accountName'
    );
  return helper.success(res, banks);
};

const save = async (req, res) => {
  try {
    const userId = req.params.userId === 'me' ? req.userId : req.params.userId;
    const { id, bankId, accountNumber, accountName } = req.body;
    const entity = {
      id,
      bankId,
      accountNumber,
      accountName,
    };
    if (id) {
      await db('bank_account').update(entity).where({ id });
    } else {
      entity.id = uuid();
      entity.employeeId = userId;
      await db('bank_account').insert(entity);
    }
    return helper.success(res, entity);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

export default {
  getListBankAccount,
  save,
};
