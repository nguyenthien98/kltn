/* eslint-disable comma-dangle */
/* eslint-disable no-useless-escape */
import { v1 as uuid } from 'uuid';
import JOI from 'joi';
import bcrypt from 'bcrypt';
import random from 'random';
import helper from '../../statusError';
import db from '../../adapters/db';

const getList = async (req, res) => {
  try {
    const getAll = await db('employee')
      .where({ deleted: false })
      .join('role', 'employee.role_id', 'role.id')
      .leftJoin('department', 'department.id', 'employee.departmentId')
      .select(
        'employee.id',
        'employee.firstname',
        'employee.lastname',
        'employee.email',
        'employee.address',
        'employee.birthday',
        'employee.phone',
        'employee.gender',
        'employee.identity_card',
        'employee.employee_code',
        'employee.avatar',
        'role.permission',
        'department.name as department',
        'department.id as departmentId'
      );
    return helper.success(res, getAll);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const get = async (req, res) => {
  try {
    const userId = req.params.userId === 'me' ? req.userId : req.params.userId;
    const getUserInfo = db('employee')
      .where({ 'employee.id': userId, 'employee.deleted': false })
      .join('role', 'employee.role_id', 'role.id')
      .first(
        'employee.id',
        'employee.firstname',
        'employee.lastname',
        'employee.email',
        'employee.address',
        'employee.birthday',
        'employee.phone',
        'employee.gender',
        'employee.identity_card',
        'employee.employee_code',
        'employee.avatar',
        'employee.departmentId',
        'role.id as roleId',
        'role.permission'
      );

    const getDepartment = db('department')
      .join('employee', 'employee.departmentId', 'department.id')
      .where({ 'employee.id': userId })
      .first('department.id', 'department.name', 'department.location');

    const getBankAccounts = db('bank_account')
      .where({ 'bank_account.employeeId': userId })
      .join('bank', 'bank_account.bankId', 'bank.id')
      .select(
        'bank_account.id',
        'bankId',
        'bank.name as bankName',
        'accountNumber',
        'accountName'
      );

    const getInsurance = db('insurance')
      .where({ 'insurance.employee_id': userId })
      .first('id', 'bookNo', 'effectiveDate', 'hospital');

    const getContracts = db('contract')
      .where({ 'contract.employeeId': userId })
      .select(
        'id',
        'contractNo',
        'startDate',
        'endDate',
        'joinDate',
        'probationStatus',
        'endProbationDate',
        'grossSalary'
      );

    const getFeedBacks = db('feedback')
      .where({ 'feedback.employeeId': userId })
      .join('employee', 'feedback.reviewerId', 'employee.id')
      .select(
        'feedback.id',
        'employee.firstname',
        'employee.lastname',
        'feedback.content',
        'feedback.reviewDate'
      );

    const getJoinProjects = db('project_member')
      .where({ 'project_member.employeeId': userId })
      .join('project', 'project_member.projectId', 'project.id')
      .select(
        'project_member.id',
        'project_member.projectId',
        'project_member.employeeId',
        'project_member.startDate',
        'project_member.endDate',
        'project.client',
        'project.name as project'
      );

    const [
      userInfo,
      department,
      bankAccounts,
      insurance,
      contracts,
      feedBacks,
      projects,
    ] = await Promise.all([
      getUserInfo,
      getDepartment,
      getBankAccounts,
      getInsurance,
      getContracts,
      getFeedBacks,
      getJoinProjects,
    ]);

    if (!userInfo) return helper.notfound(res);
    userInfo.department = department || {};
    userInfo.bankAccounts = bankAccounts || [];
    userInfo.insurance = insurance || {};
    userInfo.contracts = contracts || [];
    userInfo.feedBacks = feedBacks || [];
    userInfo.projects = projects || [];

    return helper.success(res, userInfo);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const create = async (req, res) => {
  if (!req.login) return helper.unauthorized(res);
  try {
    const { email, password, roleId } = req.body;
    const schema = JOI.object({
      email: JOI.string()
        .required()
        .email({
          minDomainSegments: 2,
          tlds: { allow: ['com', 'net'] },
        })
        .min(8)
        .max(255)
        .pattern(new RegExp(/^[a-zA-Z0-9@.]+$/)),
      password: JOI.string().required().min(8).max(100),
    });

    const validation = schema.validate({ email, password });

    if (validation.error || validation.errors) {
      return helper.badRequest(res);
    }

    const account = await db.first().table('employee').where('email', email);
    if (account) return helper.badRequest(res, 'EMAIL EXIST');

    await db('employee').insert({
      id: uuid(),
      email,
      password: await bcrypt.hash(password, 10),
      role_id: roleId,
      employee_code: random.int(1000, 9999),
    });

    return helper.success(res, 'register success');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const update = async (req, res) => {
  if (!req.login) return helper.unauthorized(res);

  try {
    const {
      id,
      firstName,
      lastName,
      address,
      birthday,
      gender,
      phone,
      identityCard,
      departmentId,
      roleId,
    } = req.body;
    if (!firstName) return helper.badRequest(res, 'ERR_COMMON');
    if (!lastName) return helper.badRequest(res, 'ERR_COMMON');
    if (!address) return helper.badRequest(res, 'ERR_COMMON');
    if (!birthday) return helper.badRequest(res, 'ERR_COMMON');
    if (!phone) return helper.badRequest(res, 'ERR_COMMON');
    if (!identityCard) return helper.badRequest(res, 'ERR_COMMON');
    if (!gender) return helper.badRequest(res, 'ERR_COMMON');
    await db('employee')
      .update({
        firstname: firstName,
        lastname: lastName,
        address,
        birthday,
        phone,
        gender,
        identity_card: identityCard,
        departmentId: departmentId || null,
        role_id: roleId,
      })
      .where({ id });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const deleteUser = async (req, res) => {
  const { userId } = req.params;
  try {
    await db('employee').update({ deleted: true }).where({ id: userId });
    return helper.success(res);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

export default {
  getList,
  get,
  create,
  update,
  deleteUser,
};
