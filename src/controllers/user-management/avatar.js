import db from '../../adapters/db';
import helper from '../../statusError';

const upload = async (req, res) => {
  try {
    const userId = req.params.userId === 'me' ? req.userId : req.params.userId;
    await db('employee')
      .update({ avatar: req.file.path })
      .where({ id: userId });
    return helper.success(res, { url: req.file.path });
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

export default { upload };
