import { v1 as uuid } from 'uuid';
import helper from '../../statusError';
import db from '../../adapters/db';

const update = async (req, res) => {
  const userId = req.params.userId === 'me' ? req.userId : req.params.userId;
  const { id, bookNo, hospital, effectiveDate } = req.body;
  const entity = {
    bookNo,
    hospital,
    employee_id: userId,
    effectiveDate,
  };

  if (id) {
    await db('insurance').update(entity).where({ id });
  } else {
    entity.id = uuid();
    await db('insurance').insert(entity);
  }

  return helper.success(res, entity);
};

export default { update };
