/* eslint-disable comma-dangle */
import { v1 as uuid } from 'uuid';
import helper from '../../statusError';
import db from '../../adapters/db';

const list = async (req, res) => {
  try {
    const { year } = req.params;
    const userId = req.params.userId === 'me' ? req.userId : req.params.userId;
    const leaves = await db('leave')
      .where({ employeeId: userId })
      .where(function () {
        this.whereBetween('startDate', [
          `${year}-01-01`,
          `${year}-12-31`,
        ]).orWhereBetween('endDate', [`${year}-01-01`, `${year}-12-31`]);
      })
      .select(
        'leave.id',
        'leave.startDate',
        'leave.endDate',
        'leave.reason',
        'leave.type',
        'leave.createdAt',
        'leave.employeeId',
        'leave.status'
      );
    return helper.success(res, leaves);
  } catch (error) {
    console.log(error);
    return helper.internalServerError(res, error);
  }
};

const create = async (req, res) => {
  const userId = req.params.userId === 'me' ? req.userId : req.params.userId;
  const entities = req.body.map((item) => ({
    id: uuid(),
    employeeId: userId,
    startDate: item.startDate,
    endDate: item.endDate,
    reason: item.reason,
    type: item.type,
    createdAt: new Date(),
  }));
  try {
    await db('leave').insert(entities);
    return helper.success(res, entities);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const cancel = async (req, res) => {
  const userId = req.params.userId === 'me' ? req.userId : req.params.userId;
  const { leaveId } = req.params;
  try {
    await db('leave')
      .update({ status: 'CANCEL' })
      .where({ id: leaveId, employeeId: userId });
    return helper.success(res, { id: leaveId });
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

export default { list, create, cancel };
