import { v1 as uuid } from 'uuid';
import helper from '../../statusError';
import db from '../../adapters/db';

const save = async (req, res) => {
  try {
    const userId = req.params.userId === 'me' ? req.userId : req.params.userId;
    const entity = {
      id: uuid(),
      reviewerId: req.userId,
      content: req.body.content,
      employeeId: userId,
      reviewDate: new Date(),
    };

    await db('feedback').insert(entity);
    entity.firstname = req.firstname;
    entity.lastname = req.lastname;
    return helper.success(res, entity);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

export default { save };
