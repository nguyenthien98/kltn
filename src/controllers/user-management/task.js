/* eslint-disable object-curly-newline */
/* eslint-disable comma-dangle */
/* eslint-disable no-useless-escape */
import { v1 as uuid } from 'uuid';
import helper from '../../statusError';
import db from '../../adapters/db';

const list = async (req, res) => {
  try {
    const { startDate, endDate } = req.query;
    const getList = await db('tasks')
      .join('task_detail', 'tasks.id', 'task_detail.taskId')
      .select()
      .where(startDate, '<=', 'task_detail.workingDate');

    return helper.success(res, getList);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const create = async (req, res) => {
  try {
    const taskId = uuid();
    const { userId } = req.params;
    const { projectId, activity, task, taskHours } = req.body;
    const data = taskHours.map((x) => ({
      taskId,
      workingDate: x.workingDate,
      workingHour: x.workingHour,
    }));
    await db.transaction(async (trx) => {
      await trx('tasks').insert({
        id: taskId,
        projectId,
        employeeId: userId,
        activity,
        task,
      });
      await trx('task_detail').insert(data);
    });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const update = async (req, res) => {
  try {
    const { id } = req.params;
    const { activity, task, taskHours } = req.body;
    const data = taskHours.map((x) => ({
      workingDate: x.workingDate,
      workingHour: x.workingHour,
    }));
    await db.transaction(async (trx) => {
      await trx('tasks')
        .update({
          activity,
          task,
        })
        .where({ id });
      await trx('task_detail').update(data).where({ taskId: id });
    });
    return helper.success(res, 'SUCCESS');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

export default {
  list,
  create,
  update,
};
