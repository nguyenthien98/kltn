import { v1 as uuid } from 'uuid';
import helper from '../../statusError';
import db from '../../adapters/db';

const save = async (req, res) => {
  try {
    const userId = req.params.userId === 'me' ? req.userId : req.params.userId;
    const { id } = req.body;
    const entity = {
      id: req.body.id,
      contractNo: req.body.contractNo,
      startDate: req.body.startDate,
      endDate: req.body.endDate,
      joinDate: req.body.joinDate,
      probationStatus: req.body.probationStatus,
      endProbationDate: req.body.endProbationDate,
      grossSalary: req.body.grossSalary,
    };
    if (id) {
      await db('contract').update(entity).where({ id });
    } else {
      entity.id = uuid();
      entity.employeeId = userId;
      await db('contract').insert(entity);
    }
    return helper.success(res, entity);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

export default { save };
