import { v1 as uuid } from 'uuid';
import helper from '../../statusError';
import db from '../../adapters/db';

const save = async (req, res) => {
  const userId = req.params.userId === 'me' ? req.userId : req.params.userId;
  const { id, projectId, startDate, endDate } = req.body;
  const entity = {
    projectId,
    startDate,
    endDate,
    employeeId: userId,
  };

  if (id) {
    await db('project_member')
      .update(entity)
      .where({ projectId, employeeId: userId });
  } else {
    entity.id = uuid();
    await db('project_member').insert(entity);
  }

  return helper.success(res, entity);
};

export default { save };
