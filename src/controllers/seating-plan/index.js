/* eslint-disable comma-dangle */
import { v1 as uuid } from 'uuid';
import helper from '../../statusError';
import db from '../../adapters/db';

const getList = async (req, res) => {
  const seats = await db('seating_plan')
    .leftJoin('employee', 'employee.id', 'seating_plan.employeeId')
    .leftJoin('department', 'department.id', 'employee.departmentId')
    .orderByRaw('length("seat"), "seat"')
    .select(
      'seating_plan.id',
      'seating_plan.floor',
      'seating_plan.seat',
      'seating_plan.employeeId',
      'employee.firstname',
      'employee.lastname',
      'department.name as department'
    );

  return helper.success(res, seats);
};

const save = async (req, res) => {
  try {
    const { id, employeeId } = req.body;
    const entity = {
      id,
      floor: req.body.floor,
      seat: req.body.seat,
      employeeId: req.body.employeeId || null,
    };

    if (entity.employeeId) {
      await db('seating_plan')
        .update({ employeeId: null })
        .where({ employeeId });
    }

    if (id) {
      await db('seating_plan').update(entity).where({ id });
    } else {
      entity.id = uuid();
      await db('seating_plan').insert(entity);
    }
    return helper.success(res, entity);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

export default {
  getList,
  save,
};
