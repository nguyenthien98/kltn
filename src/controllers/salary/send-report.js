/* eslint-disable no-console */
/* eslint-disable operator-linebreak */
/* eslint-disable comma-dangle */
import excel from 'exceljs';
import moment from 'moment';
import helper from '../../statusError';
import mail from '../../utils/mail.service';

const createReport = async (salary) => {
  const templatePath = 'src/templates/excels/personal-salary.xlsx';
  const workbook = new excel.Workbook();
  await workbook.xlsx.readFile(templatePath);
  const worksheet = workbook.getWorksheet('Sheet');

  let row = worksheet.getRow(8);
  row.getCell(3).value = moment(`${salary.time}-01`).format('MMM YYYY');
  row.commit();

  row = worksheet.getRow(9);
  row.getCell(3).value = salary.employee;
  row.commit();

  row = worksheet.getRow(10);
  row.getCell(3).value = salary.gross;
  row.commit();

  row = worksheet.getRow(14);
  row.getCell(
    2
  ).value = `Salary for ${salary.totalWorkingDate} working days in official employment`;
  row.getCell(6).value = salary.actualGross;
  row.commit();

  row = worksheet.getRow(15);
  row.getCell(6).value = salary.other;
  row.commit();

  let otherIndex = 16;
  salary.others.forEach((other) => {
    row = worksheet.getRow(otherIndex);
    row.getCell(2).value = other.description;
    row.getCell(6).value = other.isIncome ? salary.amount : -salary.amount;
    row.commit();
    otherIndex += 1;
  });

  row = worksheet.getRow(19);
  row.getCell(6).value = salary.social + salary.health + salary.unemployment;
  row.commit();

  row = worksheet.getRow(20);
  row.getCell(6).value = salary.social;
  row.commit();

  row = worksheet.getRow(21);
  row.getCell(6).value = salary.health;
  row.commit();

  row = worksheet.getRow(22);
  row.getCell(6).value = salary.unemployment;
  row.commit();

  const taxIncome = salary.actualGross +
  salary.other -
  11000000 -
  salary.social -
  salary.health -
  salary.unemployment;
  row = worksheet.getRow(26);
  row.getCell(6).value = taxIncome <= 0 ? 0 : taxIncome;
  row.commit();

  row = worksheet.getRow(28);
  row.getCell(6).value = salary.tax;
  row.commit();

  row = worksheet.getRow(30);
  row.getCell(6).value = salary.net;
  row.commit();

  row = worksheet.getRow(32);
  row.getCell(6).value =
    salary.companySocial + salary.companyHealth + salary.unemployment;
  row.commit();

  row = worksheet.getRow(33);
  row.getCell(6).value = salary.companySocial;
  row.commit();

  row = worksheet.getRow(34);
  row.getCell(6).value = salary.companyHealth;
  row.commit();

  row = worksheet.getRow(35);
  row.getCell(6).value = salary.unemployment;
  row.commit();

  const filename = `${salary.employeeCode}.${salary.time}.xlsx`;
  const path = `reports/salaries/${filename}`;
  await workbook.xlsx.writeFile(path);

  mail.sendSalaryReport(salary.time, salary.email, filename, path);
};

const send = async (req, res) => {
  const salaries = req.body;
  const generateReportTask = [];
  salaries.forEach((item) => {
    generateReportTask.push(createReport(item));
  });

  await Promise.all(generateReportTask);

  return helper.success(res);
};

export default { send };
