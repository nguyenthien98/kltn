import helper from '../../statusError';
import db from '../../adapters/db';

const update = async (req, res) => {
    if (req.body.length === 0)
        return helper.success(res);

    try {
        await db.transaction(async (trx) => {
            await trx('salary_other').where({
                time: req.body[0].time,
                employeeId: req.body[0].employeeId
            }).del();
            await trx('salary_other').insert(req.body);
        });

        return helper.success(res);
    } catch {
        return helper.internalServerError(res);
    }
};

export default {
    update
};