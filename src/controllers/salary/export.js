/* eslint-disable comma-dangle */
import excel from 'exceljs';
import moment from 'moment';

const exportSalaries = async (req, res) => {
  const salaries = req.body;
  const templatePath = 'src/templates/excels/salaries.xlsx';
  const workbook = new excel.Workbook();
  await workbook.xlsx.readFile(templatePath);
  const worksheet = workbook.getWorksheet('Salaries');

  let row = worksheet.getRow(2);
  row.getCell(6).value = salaries.reduce((a, b) => a + b.net, 0);
  row.commit();

  row = worksheet.getRow(3);
  row.getCell(6).value = salaries.reduce(
    (a, b) => a + b.social + b.companySocial,
    0
  );
  row.commit();

  row = worksheet.getRow(4);
  row.getCell(6).value = salaries.reduce(
    (a, b) => a + b.health + b.companyHealth,
    0
  );
  row.commit();

  row = worksheet.getRow(5);
  row.getCell(2).value = moment(`${salaries[0].time}-01`).format('MMM YYYY');
  row.getCell(6).value = salaries.reduce(
    (a, b) => a + b.unemployment + b.unemployment,
    0
  );
  row.commit();

  row = worksheet.getRow(6);
  row.getCell(2).value = salaries[0].submitted ? 'Submitted' : 'Estimated';
  row.getCell(6).value = salaries.reduce((a, b) => a + b.total, 0);
  row.commit();

  let rowIndex = 8;
  salaries.forEach((item) => {
    row = worksheet.getRow(rowIndex);
    row.getCell(1).value = item.employeeCode;
    row.getCell(2).value = item.employee;
    row.getCell(
      3
    ).value = `${item.totalWorkingDate}/${item.totalWorkingDateOfMonth}`;
    row.getCell(4).value = item.net;
    row.getCell(5).value = item.social + item.companySocial;
    row.getCell(6).value = item.health + item.companyHealth;
    row.getCell(7).value = item.unemployment + item.unemployment;
    row.getCell(8).value = item.tax;
    row.getCell(9).value = item.other;
    row.getCell(10).value = item.total;
    row.commit();
    rowIndex += 1;
  });

  await workbook.xlsx.write(res);
  return res.end();
};

export default { exportSalaries };
