/* eslint-disable function-paren-newline */
/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable indent */
/* eslint-disable operator-linebreak */
/* eslint-disable no-restricted-syntax */
/* eslint-disable comma-dangle */
import { v1 as uuid } from 'uuid';
import moment from 'moment';
import helper from '../../statusError';
import db from '../../adapters/db';
import DatetimeUtils from '../../utils/datetime';

const WEEK_DAYS = {
  SAT: 6,
  SUN: 0,
};

const getTotalLeaveDate = (leaves) => {
  let total = 0;
  leaves.forEach((item) => {
    total += DatetimeUtils.getTotalWorkDays(item.startDate, item.endDate);
  });

  return total;
};

const getTotalOTHours = async (userId, startDate, endDate) => {
  const days = DatetimeUtils.getAllDates(startDate, endDate).map((item) =>
    item.format('YYYY-MM-DD')
  );
  const tasks = await db('task_detail')
    .join('task', 'task_detail.taskId', 'task.id')
    .where({ 'task.employeeId': userId })
    .whereIn('workingDate', days)
    .select('task_detail.workingDate', 'task_detail.workingHour');

  const date = moment(startDate);
  let totalHours = 0;
  while (date.isSameOrBefore(endDate, 'day')) {
    const hours = tasks
      .filter((item) => item.workingDate === date.format('YYYY-MM-DD'))
      .reduce((a, b) => a + b.workingHour, 0);

    if (date.day() === WEEK_DAYS.SAT || date.day() === WEEK_DAYS.SUN) {
      totalHours += hours;
    } else if (hours > 8) {
      totalHours += hours - 8;
    }

    date.add(1, 'd');
  }

  return totalHours;
};

const getList = async (req, res) => {
  const { time } = req.query;
  const startDateOfMonth = moment(`${time}-01`);
  const endDateOfMonth = moment(startDateOfMonth).endOf('month');

  let list = await db('salary')
    .where({ time })
    .orderBy('salary.employeeCode')
    .select(
      'salary.id',
      'salary.time',
      'salary.employeeId',
      'salary.employee',
      'salary.employeeCode',
      'salary.email',
      'salary.gross',
      'salary.totalWorkingDate',
      'salary.startDate',
      'salary.endDate',
      db.raw('? as submitted', [true])
    );

  if (list.length === 0) {
    list = await db('employee')
      .join('contract', 'employee.id', 'contract.employeeId')
      .where('contract.startDate', '<=', endDateOfMonth.format('YYYY-MM-DD'))
      .whereRaw(
        `"endDate" IS NULL OR "endDate" >= '${startDateOfMonth.format(
          'YYYY-MM-DD'
        )}'`
      )
      .distinctOn('employee.employee_code')
      .orderBy('employee.employee_code')
      .select(
        'employee.id as employeeId',
        'employee.firstname',
        'employee.lastname',
        'employee.email',
        'employee.employee_code as employeeCode',
        'contract.grossSalary as gross',
        'contract.startDate',
        'contract.endDate',
        db.raw('? as time', [time]),
        db.raw('? as submitted', [false])
      );

    for (const item of list) {
      const startContractDate = moment(item.startDate);
      const endContractDate = item.endDate ? moment(item.endDate) : null;

      item.startDate =
        startContractDate < startDateOfMonth
          ? startDateOfMonth
          : startContractDate;
      item.endDate =
        endContractDate === null || endContractDate > endDateOfMonth
          ? endDateOfMonth
          : endContractDate;

      item.id = uuid();
      item.employee = `${item.firstname} ${item.lastname}`;
      item.totalWorkingDate = DatetimeUtils.getTotalWorkDays(
        item.startDate,
        item.endDate
      );
    }
  }

  list = list.filter((item) => item.startDate <= item.endDate);

  for await (const item of list) {
    const { employeeId } = item;
    const getOthers = db('salary_other')
      .where({ time, employeeId })
      .select('id', 'amount', 'description', 'isIncome', 'employeeId', 'time');

    const getLeaves = db('leave')
      .where({ employeeId, status: null, type: 'NON_PAID' })
      .where(function () {
        this.whereBetween('startDate', [
          startDateOfMonth.format('YYYY-MM-DD'),
          endDateOfMonth.format('YYYY-MM-DD'),
        ]).orWhereBetween('endDate', [
          startDateOfMonth.format('YYYY-MM-DD'),
          endDateOfMonth.format('YYYY-MM-DD'),
        ]);
      })
      .select('leave.startDate', 'leave.endDate', 'leave.reason');

    const [others, leaves] = await Promise.all([getOthers, getLeaves]);

    item.others = others;
    item.leaves = leaves;
    item.submitted = item.submitted === 'true';
    item.totalWorkingDateOfMonth = DatetimeUtils.getTotalWorkDays(
      startDateOfMonth,
      endDateOfMonth
    );

    if (!item.submitted) {
      item.totalWorkingDate -= getTotalLeaveDate(leaves);
      const otHours = await getTotalOTHours(
        item.employeeId,
        startDateOfMonth,
        endDateOfMonth
      );
      if (otHours) {
        item.others.push({
          time,
          amount: (item.gross / (item.totalWorkingDateOfMonth * 8)) * otHours,
          description: `OT ${otHours} hour(s)`,
          isIncome: true,
          employeeId: item.employeeId,
        });
      }
    }
  }

  return helper.success(res, list);
};

const save = async (req, res) => {
  const salaries = req.body.map((item) => ({
    id: item.id,
    time: item.time,
    employeeId: item.employeeId,
    employee: item.employee,
    employeeCode: item.employeeCode,
    totalWorkingDate: item.totalWorkingDate,
    gross: item.gross,
    startDate: item.startDate,
    endDate: item.endDate,
  }));

  let others = [];
  req.body.forEach((element) => {
    others = others.concat(
      element.others
        .filter((item) => !item.id)
        .map((item) => ({
          id: uuid(),
          amount: item.amount,
          description: item.description,
          employeeId: item.employeeId,
          isIncome: item.isIncome,
          time: item.time,
        }))
    );
  });

  try {
    await db.transaction(async (trx) => {
      await trx('salary').insert(salaries);
      await trx('salary_other').insert(others);
    });
    return helper.success(res);
  } catch {
    return helper.internalServerError();
  }
};

export default {
  getList,
  save,
};
