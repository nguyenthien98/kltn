/* eslint-disable no-useless-escape */
import bcrypt from 'bcrypt';
import JOI from 'joi';
import jwt from 'jsonwebtoken';
import check from 'is_js';
import db from '../../adapters/db';
import helper from '../../statusError';

const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const schema = JOI.object({
      email: JOI.string()
        .email({
          minDomainSegments: 2,
          tlds: { allow: ['com', 'net'] },
        })
        .required()
        .min(6)
        .max(255)
        .pattern(new RegExp(/^[a-zA-Z0-9@.-]+$/)),
      password: JOI.string().required().min(6).max(255),
    });

    const validation = schema.validate({ email, password });
    if (validation.error || validation.errors) {
      return helper.badRequest(res, 'USER_OR_PASSWORD_INVALID');
    }

    const user = await db
      .first('id', 'email', 'password', 'role_id', 'departmentId')
      .table('employee')
      .where({ email, deleted: false });

    if (!check.existy(user)) return helper.badRequest(res, 'USER_OR_PASSWORD_INVALID');

    const pwd = await bcrypt.compare(password, user.password);
    if (!pwd) return helper.badRequest(res, 'USER_OR_PASSWORD_INVALID');

    const accessToken = jwt.sign({ data: email }, 'secret', {
      expiresIn: '2y',
    });
    const role = await db('role')
      .where({ id: user.role_id })
      .first('permission');

    return helper.success(res, {
      message: 'login success',
      token: accessToken,
      id: user.id,
      departmentId: user.departmentId,
      role: role.permission,
    });
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};
const logout = async (req, res) => {
  try {
    if (!req.login) return helper.unauthorized(res, 'login please');
    return helper.success(res, 'ok');
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const changePassword = async (req, res) => {
  if (!req.login) return helper.unauthorized(res, 'login please');
  const { email } = req;
  try {
    const { oldPassword, newPassword } = req.body;
    const user = await db
      .first('id', 'email', 'password', 'role_id')
      .table('employee')
      .where('email', email);
    if (!check.existy(user)) return helper.badRequest(res, 'USER_INVALID');
    const pwd = await bcrypt.compare(oldPassword, user.password);
    if (!pwd) return helper.badRequest(res, 'PASSWORD_INVALID');

    const hashPassword = await bcrypt.hash(newPassword, 10);
    await db('employee').update({ password: hashPassword }).where({ email });
    return helper.success(res, { message: 'success' });
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

const getSalary = async (req, res) => {
  try {
    const getone = await db('employee').first();
    return helper.success(res, getone);
  } catch (error) {
    return helper.internalServerError(res, error);
  }
};

export default {
  login,
  logout,
  changePassword,
  getSalary,
};
